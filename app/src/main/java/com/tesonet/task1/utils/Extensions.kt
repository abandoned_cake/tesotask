package com.tesonet.task1.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.widget.ImageViewCompat
import com.google.android.material.snackbar.Snackbar
import com.tesonet.task1.R

object Extensions {

    fun SharedPreferences.saveString(key: String, value: String?) {
        this.edit().apply {
            putString(key, value)
            apply()
        }
    }

    fun <T> Activity.switchActivity(target: Class<T>, enter: Boolean? = true) {
        val intent = Intent(this, target)
        startActivity(intent)
        when (enter) {
            true -> overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            false -> overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            else -> overridePendingTransition(0, 0)
        }
        this.finish()
    }

    fun ImageView.changeTint(@ColorRes colorId: Int) {
        ImageViewCompat.setImageTintList(
            this,
            ColorStateList.valueOf(resources.getColor(colorId, this.context.theme))
        )
    }

    fun View.showSnackbar(
        @StringRes message: Int,
        length: Int = Snackbar.LENGTH_INDEFINITE,
        action: () -> Unit
    ) {
        val snack = Snackbar.make(this, this.context.getString(message), length)
        snack.setAction(R.string.retry) { action() }
        snack.show()
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}

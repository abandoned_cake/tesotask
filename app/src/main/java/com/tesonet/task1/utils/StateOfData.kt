package com.tesonet.task1.utils

import androidx.annotation.ColorRes

enum class StateOfData(@ColorRes val colorInt: Int) {
    Live(android.R.color.holo_green_light),
    Cached(android.R.color.holo_red_light),
    Other(android.R.color.holo_blue_dark),
}

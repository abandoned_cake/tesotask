package com.tesonet.task1.di

import com.tesonet.task1.function.fragments.details.DetailsFragment
import com.tesonet.task1.function.fragments.list.ListFragment
import com.tesonet.task1.function.fragments.login.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeListFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment
}

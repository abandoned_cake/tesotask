package com.tesonet.task1.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.tesonet.task1.data.db.LocalDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideSharedPrefs(appContext: Context): SharedPreferences =
        appContext.getSharedPreferences("AppPrefs", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    fun provideDatabase(context: Context): LocalDb = LocalDb.getInstance(context)
}

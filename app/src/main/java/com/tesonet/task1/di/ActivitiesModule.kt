package com.tesonet.task1.di

import com.tesonet.task1.function.activities.LoginActivity
import com.tesonet.task1.function.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun contributeMainActivityModule(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun contributeLoginActivity(): LoginActivity
}

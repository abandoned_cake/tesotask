package com.tesonet.task1.function.activities

import android.content.SharedPreferences
import android.os.Bundle
import com.tesonet.task1.R
import com.tesonet.task1.function.base.BaseActivity
import com.tesonet.task1.utils.Constants
import com.tesonet.task1.utils.Extensions.switchActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private val tokenListener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            if (key == Constants.token) {
                val noToken = sharedPreferences.getString(key, null).isNullOrBlank()
                if (noToken) {
                    this.switchActivity(LoginActivity::class.java, false)
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        sharedPreferences.registerOnSharedPreferenceChangeListener(tokenListener)
    }

    override fun onStop() {
        super.onStop()
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(tokenListener)
    }
}

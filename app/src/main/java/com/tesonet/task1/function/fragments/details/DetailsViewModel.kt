package com.tesonet.task1.function.fragments.details

import com.airbnb.mvrx.FragmentViewModelContext
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.tesonet.task1.function.base.MvRxViewModel

class DetailsViewModel @AssistedInject constructor(
    @Assisted state: State
) : MvRxViewModel<State>(state) {

    @AssistedInject.Factory
    interface Factory {
        fun create(state: State): DetailsViewModel
    }

    companion object : MvRxViewModelFactory<DetailsViewModel, State> {
        override fun create(viewModelContext: ViewModelContext, state: State): DetailsViewModel? {
            val fragment =
                (viewModelContext as FragmentViewModelContext).fragment<DetailsFragment>()
            return fragment.viewModelFactory.create(state)
        }
    }
}

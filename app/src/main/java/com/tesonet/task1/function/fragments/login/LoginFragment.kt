package com.tesonet.task1.function.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import com.tesonet.task1.R
import com.tesonet.task1.function.activities.MainActivity
import com.tesonet.task1.function.base.BaseFragment
import com.tesonet.task1.utils.Extensions.switchActivity
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: LoginViewModel.Factory

    private val viewModel: LoginViewModel by fragmentViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        syncState()
        setListeners()
    }

    @Suppress("MagicNumber")
    override fun invalidate() = withState(viewModel) { state ->
        Timber.d("state $state")
        loginProgressbar.isVisible = state.logInStatus is Loading
        loginButton.isEnabled =
            (state.logInStatus !is Loading) && state.userName.length > 5 && state.password.length > 5
    }

    private fun setListeners() {
        loginButton.setOnClickListener { viewModel.onEvent(LoginViewModel.Events.Login) }
        loginName.editText?.doOnTextChanged { text, _, _, _ ->
            clearError()
            viewModel.onEvent(LoginViewModel.Events.UserNameChanged(text?.toString().orEmpty()))
        }
        loginPassword.editText?.doOnTextChanged { text, _, _, _ ->
            clearError()
            viewModel.onEvent(LoginViewModel.Events.PasswordChanged(text?.toString().orEmpty()))
        }
        viewModel.asyncSubscribe(
            State::logInStatus,
            onFail = { error -> showError(error) }) { loggedIn() }

        loginPassword.editText?.setOnEditorActionListener { v, actionId, event ->
            Timber.d("listener precall $event, $actionId")
            if (actionId == EditorInfo.IME_ACTION_DONE && loginButton.isEnabled) {
                Timber.d("listener called")
                loginButton.callOnClick()
            }
            false
        }
    }

    private fun syncState() {
        withState(viewModel) {
            loginName.editText?.setText(it.userName)
            loginPassword.editText?.setText(it.password)
        }
    }

    @Suppress("MagicNumber")
    private fun showError(error: Throwable) {
        if (error is HttpException && error.code() == 401) {
            loginPassword.error = getString(R.string.errorLogin)
        } else {
            loginPassword.error = getString(R.string.errorGeneral)
        }
    }

    private fun clearError() {
        loginPassword.error = null
    }

    private fun loggedIn() = requireActivity().switchActivity(MainActivity::class.java)
}

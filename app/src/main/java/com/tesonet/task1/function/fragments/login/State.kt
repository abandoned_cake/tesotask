package com.tesonet.task1.function.fragments.login

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.PersistState
import com.airbnb.mvrx.Uninitialized
import com.tesonet.task1.data.api.models.Token

data class State(
    @PersistState
    val userName: String = "",
    @PersistState
    val password: String = "",
    val logInStatus: Async<Token> = Uninitialized
) : MvRxState

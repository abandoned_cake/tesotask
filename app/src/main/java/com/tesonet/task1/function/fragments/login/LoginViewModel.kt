package com.tesonet.task1.function.fragments.login

import com.airbnb.mvrx.FragmentViewModelContext
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.tesonet.task1.data.DataSource
import com.tesonet.task1.function.base.MvRxViewModel
import io.reactivex.schedulers.Schedulers

class LoginViewModel @AssistedInject constructor(
    @Assisted state: State,
    private val dataSource: DataSource
) : MvRxViewModel<State>(state) {

    @AssistedInject.Factory
    interface Factory {
        fun create(state: State): LoginViewModel
    }

    companion object : MvRxViewModelFactory<LoginViewModel, State> {
        @JvmStatic
        override fun create(viewModelContext: ViewModelContext, state: State): LoginViewModel {
            val fragment = (viewModelContext as FragmentViewModelContext).fragment<LoginFragment>()
            return fragment.viewModelFactory.create(state)
        }
    }

    fun onEvent(event: Events) = withState {
        when (event) {
            Events.Login -> login()
            is Events.UserNameChanged -> setState { copy(userName = event.userName) }
            is Events.PasswordChanged -> setState { copy(password = event.password) }
        }
    }

    sealed class Events {
        object Login : Events()
        data class UserNameChanged(val userName: String) : Events()
        data class PasswordChanged(val password: String) : Events()
    }

    private fun login() = withState { state ->
        if (state.logInStatus is Loading) return@withState
        dataSource
            .login(userName = state.userName, password = state.password)
            .subscribeOn(Schedulers.io())
            .execute { copy(logInStatus = it) }
    }
}

package com.tesonet.task1.function.fragments.list

import android.os.Bundle
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.tesonet.task1.data.db.model.LocalServer

class ServersDiffCallback(
    private val oldList: List<LocalServer>,
    private val newList: List<LocalServer>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val old = oldList[oldPosition]
        val new = newList[newPosition]

        return old.name == new.name && old.distance == new.distance
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {

        val old = oldList[oldPosition]
        val new = newList[newPosition]
        val diffBundle = Bundle()

        if (new.name != old.name) {
            diffBundle.putString(KEY_NAME, new.name)
        }

        return diffBundle.takeUnless { it.isEmpty }
    }

    companion object {
        const val KEY_NAME = "name"
    }
}

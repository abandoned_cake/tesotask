package com.tesonet.task1.function.fragments.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MvRx
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import com.tesonet.task1.R
import com.tesonet.task1.utils.StateOfData
import com.tesonet.task1.data.db.model.LocalServer
import com.tesonet.task1.function.base.BaseFragment
import com.tesonet.task1.function.fragments.details.InitialDetailsState
import com.tesonet.task1.utils.Extensions.changeTint
import com.tesonet.task1.utils.Extensions.showSnackbar
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class ListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ListViewModel.Factory

    private val viewModel: ListViewModel by fragmentViewModel()

    private var adapter: ListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logoutButton.setOnClickListener { viewModel.logout() }
        listRefresh.setOnRefreshListener { viewModel.getData(true) }
        serversList.layoutManager = LinearLayoutManager(view.context)
    }

    override fun invalidate() = withState(viewModel) { state ->
        setOrigin(state.list.invoke()?.stateOfData ?: StateOfData.Other)
        setItems(state.data?.servers ?: emptyList())
        listProgressbar.isVisible = state.list is Loading
        listRefresh.isRefreshing = state.refreshing
        if (state.list is Fail) {
            listCoordinator.showSnackbar(
                message = R.string.errorGeneral,
                action = { viewModel.getData(freshData = true, refreshing = false) }
            )
        }
    }

    private fun setItems(list: List<LocalServer>) {
        if (serversList.adapter == null) {
            val divider = DividerItemDecoration(serversList.context, LinearLayout.VERTICAL)
            serversList.addItemDecoration(divider)
            adapter = ListAdapter { server -> navigateToDetails(server) }
            serversList.adapter = adapter
        }
        adapter?.setData(list)
    }

    private fun navigateToDetails(localServer: LocalServer) {
        val bundle =
            bundleOf(MvRx.KEY_ARG to InitialDetailsState(localServer.name, localServer.distance))
        navigator.navigate(R.id.action_fragmentList_to_fragmentDetails, bundle)
    }

    private fun setOrigin(stateOfData: StateOfData) = listOnlineIndicator.changeTint(stateOfData.colorInt)
}

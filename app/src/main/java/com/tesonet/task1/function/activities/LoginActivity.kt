package com.tesonet.task1.function.activities

import android.os.Bundle
import com.tesonet.task1.R
import com.tesonet.task1.data.DataSource
import com.tesonet.task1.function.base.BaseActivity
import com.tesonet.task1.utils.Extensions.switchActivity
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject
    lateinit var dataSource: DataSource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val token = dataSource.getToken()
        if (token != null) {
            this.switchActivity(MainActivity::class.java, null)
        }
        setContentView(R.layout.activity_login)
    }
}

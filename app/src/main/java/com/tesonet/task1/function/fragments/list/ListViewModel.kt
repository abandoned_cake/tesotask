package com.tesonet.task1.function.fragments.list

import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.FragmentViewModelContext
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.Uninitialized
import com.airbnb.mvrx.ViewModelContext
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.tesonet.task1.data.DataSource
import com.tesonet.task1.function.base.MvRxViewModel
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class ListViewModel @AssistedInject constructor(
    @Assisted state: ListState,
    private val dataSource: DataSource
) : MvRxViewModel<ListState>(state) {

    init {
        getData(freshData = true)
    }

    @AssistedInject.Factory
    interface Factory {
        fun create(state: ListState): ListViewModel
    }

    companion object : MvRxViewModelFactory<ListViewModel, ListState> {
        @JvmStatic
        override fun create(viewModelContext: ViewModelContext, state: ListState): ListViewModel {
            val fragment = (viewModelContext as FragmentViewModelContext).fragment<ListFragment>()
            return fragment.viewModelFactory.create(state)
        }
    }

    fun getData(freshData: Boolean, refreshing: Boolean = false) {
        dataSource
            .getServers(fromBE = freshData)
            .subscribeOn(Schedulers.io())
            .execute { state ->
                when (state) {
                    Uninitialized -> copy(list = state)
                    is Loading -> copy(list = state, refreshing = refreshing)
                    is Success -> copy(data = state.invoke(), list = state, refreshing = false)
                    is Fail -> copy(list = state, refreshing = false)
                }
            }
    }

    fun logout() = dataSource.logout().subscribe({}, { error -> Timber.d("Error $error") })
}

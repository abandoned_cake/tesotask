package com.tesonet.task1.function.fragments.details

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class InitialDetailsState(val name: String, val distance: Int) : Parcelable

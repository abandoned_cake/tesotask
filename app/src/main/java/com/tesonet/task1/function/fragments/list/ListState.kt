package com.tesonet.task1.function.fragments.list

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.tesonet.task1.data.DataSource

data class ListState(
    val refreshing: Boolean = false,
    val data: DataSource.ServerData? = null,
    val list: Async<DataSource.ServerData> = Uninitialized
) : MvRxState

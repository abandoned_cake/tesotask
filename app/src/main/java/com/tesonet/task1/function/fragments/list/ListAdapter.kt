package com.tesonet.task1.function.fragments.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.tesonet.task1.R
import com.tesonet.task1.data.db.model.LocalServer
import kotlinx.android.synthetic.main.item_server.view.*

class ListAdapter(private val eventListener: (LocalServer) -> Unit) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    private val servers: MutableList<LocalServer> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_server, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val server = servers[position]
        holder.bind(server)
        holder.itemView.setOnClickListener {
            eventListener(servers[holder.adapterPosition])
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val bundle = payloads.firstOrNull() as? Bundle
            val newName = bundle?.getString(ServersDiffCallback.KEY_NAME)
            newName?.let { holder.bindPartial(it) }
        }
    }

    fun setData(servers: List<LocalServer>) {
        val diffCallback = ServersDiffCallback(this.servers, servers)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)
        this.servers.clear()
        this.servers.addAll(servers)
    }

    override fun getItemCount(): Int = servers.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: MaterialTextView = itemView.itemServerName

        fun bind(localServer: LocalServer) {
            name.text = localServer.name
        }

        fun bindPartial(newName: String) {
            name.text = newName
        }
    }
}

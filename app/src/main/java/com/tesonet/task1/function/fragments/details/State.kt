package com.tesonet.task1.function.fragments.details

import com.airbnb.mvrx.MvRxState

data class State(val name: String, val distance: Int) : MvRxState {
    constructor(args: InitialDetailsState) : this(name = args.name, distance = args.distance)
}

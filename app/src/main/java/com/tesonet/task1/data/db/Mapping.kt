package com.tesonet.task1.data.db

import com.tesonet.task1.data.api.models.Server
import com.tesonet.task1.data.db.model.LocalServer

object Mapping {
    fun Server.toLocal(): LocalServer {
        return LocalServer(name = this.name, distance = this.distance)
    }

    fun LocalServer.toServer(): Server {
        return Server(name = this.name, distance = this.distance)
    }
}

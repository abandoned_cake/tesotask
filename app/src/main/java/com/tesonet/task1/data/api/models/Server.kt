package com.tesonet.task1.data.api.models

import com.squareup.moshi.Json

data class Server(
    @Json(name = "distance")
    val distance: Int,
    @Json(name = "name")
    val name: String
)

package com.tesonet.task1.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tesonet.task1.data.db.dao.ServersDao
import com.tesonet.task1.data.db.model.LocalServer

@Database(entities = [LocalServer::class], version = 1)
abstract class LocalDb : RoomDatabase() {

    abstract fun serverDao(): ServersDao

    companion object {

        @Volatile
        private var INSTANCE: LocalDb? = null

        fun getInstance(context: Context): LocalDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context)
                    .also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                LocalDb::class.java, "LocalDb.db"
            )
                .fallbackToDestructiveMigration()
                .build()
    }
}

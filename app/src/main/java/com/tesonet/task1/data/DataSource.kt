package com.tesonet.task1.data

import android.content.SharedPreferences
import com.tesonet.task1.data.api.TesonetApi
import com.tesonet.task1.data.api.models.Token
import com.tesonet.task1.data.api.models.TokenRequest
import com.tesonet.task1.data.db.LocalDb
import com.tesonet.task1.data.db.Mapping.toLocal
import com.tesonet.task1.data.db.model.LocalServer
import com.tesonet.task1.utils.Constants
import com.tesonet.task1.utils.Extensions.saveString
import com.tesonet.task1.utils.StateOfData
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DataSource @Inject constructor(
    private val localDb: LocalDb,
    private val tesonetApi: TesonetApi,
    private val sharedPreferences: SharedPreferences
) {

    data class ServerData(val stateOfData: StateOfData, val servers: List<LocalServer>)

    fun login(userName: String, password: String): Single<Token> {
        return tesonetApi
            .getToken(TokenRequest(username = userName, password = password))
            .doOnSuccess { sharedPreferences.saveString(Constants.token, it.token) }
            .subscribeOn(Schedulers.io())
    }

    fun logout(): Completable {
        return Completable.fromCallable {
            localDb.serverDao().deleteAllServers()
            sharedPreferences.edit().apply {
                putString(Constants.token, null)
                apply()
            }
        }
            .subscribeOn(Schedulers.io())
    }

    fun getServers(fromBE: Boolean = false): Single<ServerData> {
        val token = sharedPreferences.getString(Constants.token, null)
        return if (token == null) {
            Single.error(NullPointerException("Token is null"))
        } else {
            return if (fromBE) {
                getServersBE(token)
                    .onErrorResumeNext(getServersLocal())
            } else {
                getServersLocal()
            }
        }
    }

    private fun getServersBE(token: String): Single<ServerData> {
        return tesonetApi
            .getServers(token)
            .flatMap { servers ->
                localDb
                    .serverDao()
                    .updateData(servers.map { server -> server.toLocal() })
                localDb.serverDao().getServersAsSingle()
            }.map { ServerData(stateOfData = StateOfData.Live, servers = it) }
            .subscribeOn(Schedulers.io())
    }

    private fun getServersLocal(): Single<ServerData> {
        return localDb
            .serverDao()
            .getServersAsSingle()
            .map { ServerData(stateOfData = StateOfData.Cached, servers = it) }
            .subscribeOn(Schedulers.io())
    }

    fun getToken(): String? {
        return sharedPreferences.getString(Constants.token, null)
    }
}

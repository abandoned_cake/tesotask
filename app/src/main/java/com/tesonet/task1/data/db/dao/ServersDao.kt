package com.tesonet.task1.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.tesonet.task1.data.db.model.LocalServer
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ServersDao {

    @Query("SELECT * FROM servers WHERE name = :name")
    fun getServer(name: String): Flowable<List<LocalServer>>

    @Query("SELECT * FROM servers")
    fun getServers(): Observable<List<LocalServer>>

    @Query("SELECT * FROM servers")
    fun getServersAsSingle(): Single<List<LocalServer>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertServer(server: LocalServer): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertServers(servers: List<LocalServer>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertServers1(servers: List<LocalServer>)

    @Query("DELETE FROM servers")
    fun deleteAllServers()

    @Transaction
    fun updateData(servers: List<LocalServer>) {
        deleteAllServers()
        insertServers1(servers)
    }
}

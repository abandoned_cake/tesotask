package com.tesonet.task1.data.api.models

import com.squareup.moshi.Json

data class TokenRequest(
    @Json(name = "password")
    val password: String,
    @Json(name = "username")
    val username: String
)

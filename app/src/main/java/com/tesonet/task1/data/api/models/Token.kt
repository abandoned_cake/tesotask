package com.tesonet.task1.data.api.models

import com.squareup.moshi.Json

data class Token(
    @Json(name = "token")
    val token: String
)

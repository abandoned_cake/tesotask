package com.tesonet.task1.data.api

import com.tesonet.task1.data.api.models.Server
import com.tesonet.task1.data.api.models.Token
import com.tesonet.task1.data.api.models.TokenRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface TesonetApi {

    @GET("servers")
    fun getServers(@Header("Authorization") token: String): Single<List<Server>>

    @POST("tokens")
    fun getToken(@Body tokenRequest: TokenRequest): Single<Token>
}
